
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">fn-weixin</h1>
<h4 align="center">一个轻量级微信公众号与小程序消息接入Api组件【极速开发】</h4>
﻿<p align="center">
 <img src="https://img.shields.io/badge/version-2.3.0-green" alt="Fn">
 <img src="https://img.shields.io/badge/License-Apache--2.0-green" alt="Apache2.0"><br/>
 <img src="https://img.shields.io/badge/Maven-3.5.2-blue" alt="Maven">
<img src="https://img.shields.io/badge/java--version-1.8-blue">
<img src="https://img.shields.io/badge/springboot--version-2.6.2-blue">
</p>

#### 介绍

jfinal weixin 的 spring boot starter，这个starter是为了方便用户快速接入微信公众号与小程序消息。
具体demo请查看：[fn-weixin-demo](https://gitee.com/FnTop/fn-weixin-demo) 和 [JFinal-weixin文档](https://gitee.com/jfinal/jfinal-weixin/wikis/pages?title=Home)
。内置SpringBoot2.6.2、Lombok，开包即用,搭建项目引用该依赖即可实现SpringBoot项目开发，无需考虑SpringBoot依赖。配置、缓存弹性扩展。



#### 使用说明


##### 兼容性
| version | spring boot version | java version |
|:-------:|:-------------------:|:------------:|
|  2.3.0  |        2.6.2        |     1.8      |

##### Maven


```
<dependency>
    <groupId>cn.fntop</groupId>
    <artifactId>fn-weixin</artifactId>
    <version>2.3.0</version>
</dependency>

```

##### 公众号
1. 继承`FnMsgControllerAdapter`，实现需要重写的消息。

2. 类添加注解`@FnMsg`，注解value为你的消息地址，使用/weixin/wx，已经组合[@RequestMapping和@Controller]

##### 小程序
1. 继承`FnApiMsgController`，实现需要重写的消息。

2. 添加注解`@FnMsg`，注解value为你的消息地址，使用/weixin/wxa，已经组合[@RequestMapping和@Controller]

##### Api

添加该注解的类直接放行或者@FnRestApi
1. 类添加`@FnApi`，注解value为你的消息地址，使用/weixin/api，已经组合[@RequestMapping和@Controller]

### 配置

```
fn:
  weixin:
    dev-mode: true #设为开发模式可以在开发阶段输出请求交互的 xml 与 json 数据，并且不会进行签名验证
    accessTokenCache: fn-wexin #避免本地将线上AccessToken冲掉,上线设置其他值或者改变dev-mode值
    wx-configs: #公众号
      - app-id: : wxc03e*********d1e70
        app-secret: : 11ed9*********e*********a42b2b5a
        token: 123456
        encoding-aes-key: : xxx
        encrypt-message: : true  #消息是否加密
    wxa-configs: #小程序
      - app-id: wx*************b3dcb
        app-secret: eec6**********bd10895bace0579
    app-id-key: appId  #决定用小程序还是公众号,请求如下：/weixin/wx?appId=xxx 
    url-patterns: /weixin/*  #默认放行路径，基本接口路径 @FnMsg value 值必须在该路径
```
#### 友善提示
![img.png](img.png)

#### 配置说明
| 配置项                          | 默认值       | 说明                                                                                                     |
|:-----------------------------|:----------|:-------------------------------------------------------------------------------------------------------|
| fn.weixin.access-token-cache | fn-weixin-cache#9845s  | 缓存名，默认fn-weixin-cache#9845s                                                                            |
| fn.weixin.app-id-key         | appId     | 多公众号时，appIdKey模式【适用同时开发公众号和小程序】，如：/weixin/wx?appId=xxx                                                 |
| fn.weixin.auto-filter        | false     | 是否开启自动筛选模式【适用只开发公众号或小程序】,自动筛选模式和appIdkey模式相反，appIdKey模式是前端决定用哪个，而自动筛选是通过appId后端判断用哪个，筛选模式会覆盖appIdKey模式 |
| fn.weixin.app-id         | null      | 自动筛选模式的appId，如果开启自动筛选，不填默认第一个公众号或小程序                                                                   |
| fn.weixin.dev-mode           | false     | 开发模式，不签名验证直接请求通过，且会输出请求交互                                                                              |
| fn.weixin.url-patterns       | /weixin/* | weixin 消息处理spring拦截器url前缀                                                                              |
| fn.weixin.wx-configs         | 公众号的配置    | 多公众号配置，存在多个配置需要开启appIdKey模式或自动筛选模式，都不使用则默认使用第一个配置项                                                     |
| fn.weixin.wxa-configs        | 小程序配置     | 多小程序配置，存在多个配置需要开启appIdKey模式或自动筛选模式，都不使用则默认使用第一个配置项                                                     |

### 自定义公众号&小程序配置
实现 `WxConfigLoader` 即可，可以从数据库中获取。

```java
/**
 * 微信配置加载器，用于自定义实现
 * #order值大于1即可扩展其他配置，默认等于1使用yml配置
 */
@Configuration
@Order(1)
@RequiredArgsConstructor
public class WxConfigDefaultLoader implements WxConfigLoader {

    private final FnConfig config;
    @Override
    public List<ApiConfig> loadWx() {
        // 公众号
        return config.getWxConfigs();
    }

    @Override
    public List<WxaConfig> loadWxa() {
        // 小程序
        return config.getWxaConfigs();
    }
}
```

### 自定义缓存配置
实现 `IAccessTokenCache` 即可

```
/**
 * 基于 spring cache 的 weixin token 缓存
 * #order值大于1即可扩展其他配置，默认等于1使用SpringAccessTokenCache
 */
@RequiredArgsConstructor
@Configuration
@Order(1)
public class SpringAccessTokenCache implements IAccessTokenCache {
    private final FnConfig config;
    private final static String ACCESS_TOKEN_PREFIX = "fn:token:";
    private final CacheManager cacheManager;
    private final FnConfig properties;

    private String getKey() {
        StringBuilder buf = new StringBuilder().append(ACCESS_TOKEN_PREFIX);
        if (config.getDevMode()) {
            buf.append("dev");
        }
        return buf.toString();
    }

    @Override
    public String get(String key) {
        return getCache().get(getKey() + key, String.class);
    }

    @Override
    public void set(String key, String jsonValue) {
        getCache().put(getKey() + key, jsonValue);
    }

    @Override
    public void remove(String key) {
        getCache().evict(getKey() + key);
    }

    private Cache getCache() {
        String accessTokenCacheName = properties.getAccessTokenCache();
        Cache cache = cacheManager.getCache(accessTokenCacheName);
        return Objects.requireNonNull(cache, "AccessToken cache is null.");
    }

}
```


### 常见问题

#### 使用阿里云镜像无法拉取

尝试如下代码，central没用的话改成`*`或`all`试试

```
    <mirrors>	
		<mirror>
            <id>alimaven</id>
            <name>aliyun maven</name>
            <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
            <mirrorOf>central</mirrorOf>
        </mirror>
    </mirrors>
```


### 开源推荐
- Spring boot 微服务，高效开发之 mica 工具集：[https://gitee.com/596392912/mica](https://gitee.com/596392912/mica)
- `Avue` 一款基于vue可配置化的神奇框架：[https://gitee.com/smallweigit/avue](https://gitee.com/smallweigit/avue)
- `pig` 宇宙最强微服务（架构师必备）：[https://gitee.com/log4j/pig](https://gitee.com/log4j/pig)
- `SpringBlade` 完整的线上解决方案（企业开发必备）：[https://gitee.com/smallc/SpringBlade](https://gitee.com/smallc/SpringBlade)
- `IJPay` 支付SDK让支付触手可及：[https://gitee.com/javen205/IJPay](https://gitee.com/javen205/IJPay)

### 更新说明
#### 2023-03-16 <img src="https://img.shields.io/badge/version-2.3.0-green">
- 稳定版本，可对接所有公众号接口，具体查看fn-weixin-demo
- 解决对接公众号除模板消息外其他接口异常问题，同时更新对应的小程序接口问题
- 新增自动筛选模式
- @RestFnApi 更名为@FnRestApi

#### 2023-03-16 <img src="https://img.shields.io/badge/version-2.2.6-green">
- 可用不稳定版本，可对接公众号模板消息，具体查看fn-weixin-demo
- 解决WeixinAppConfig初始化问题
- 新增@RestFnApi注解，封装了@RestController+@RequestMapping
- WxConfigDefaultLoader新增日志
- 删除yml配置注释，注释统一放到配置类FnConfig
- 解决引入依赖需要重新引入lombok问题

#### 2023-03-12 <img src="https://img.shields.io/badge/version-2.1.5-green">
- 可用不稳定版本【可正常启动项目，但不能对接公众号或小程序】
- 解决引用@RequiredArgsConstructor注解需要导入lombok问题

#### 2023-03-12 <img src="https://img.shields.io/badge/version-2.1.4-green">
- 不可用版本
- 打包排除启动类和application.yml文件
- 更新Lisence
- spring-boot-starter-parent更换为spring-boot-dependencies
- 更改jfinal、jfinal-weinx的scope为compile

#### 2023-03-09 <img src="https://img.shields.io/badge/version-2.1.3-green">
- 不可用版本
- 解决引入依赖SpringBoot项目启动报错

#### 2023-03-09 <img src="https://img.shields.io/badge/version-2.1.2-green">
- @FnApi、@FnApi封装升级

#### 2023-03-09 <img src="https://img.shields.io/badge/version-2.1.1-green">
- 修复 默认配置加载器失败问题。
- 升级 java 到 1.8 
- 升级 Spring boot 到 2.6.2
- 升级 jfinal 到 4.9.17 
- 升级 jfinal-weixin 到 3.4 
- 升级 lombok 到 1.18.22 
