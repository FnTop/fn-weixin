package cn.fntop.weixin.config;

import com.jfinal.weixin.sdk.api.ApiConfig;
import com.jfinal.wxaapp.WxaConfig;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

import java.util.List;

/**
 * 微信默认yml配置加载器
 *
 * @author fn
 */
@Configuration
@Order(1)
@RequiredArgsConstructor
@Slf4j
public class WxConfigDefaultLoader implements WxConfigLoader {

	private final FnConfig config;
	@Override
	public List<ApiConfig> loadWx() {
		// 公众号
		log.info("默认配置实现小程序【WxConfigDefaultLoader】");
		return config.getWxConfigs();
	}

	@Override
	public List<WxaConfig> loadWxa() {
		// 小程序
		log.info("默认配置实现公众号【WxConfigDefaultLoader】");
		return config.getWxaConfigs();
	}
}