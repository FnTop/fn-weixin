package cn.fntop.weixin.config;

import com.jfinal.weixin.sdk.api.ApiConfig;
import com.jfinal.wxaapp.WxaConfig;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * @author fn
 * @description
 * @date 2023/3/5 22:49
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "fn.weixin")
public class FnConfig {
    /**
     * 默认放行路径，基本接口路径 @FnApi,@FnMsg value 值必须在该路径
     */
    private String urlPatterns = "/weixin/*";
    /**
     * 决定用小程序还是公众号,决定用小程序还是公众号,请求如下：/weixin/wx?appId=xxx
     */
    private String appIdKey = "appId";
    /**
     * 开启自动筛选模式,自动筛选模式和appIdkey模式相反，appIdKey模式是前端决定用哪个，而自动筛选是通过appId后端判断用哪个，筛选模式会覆盖appIdKey模式
     */
    private Boolean autoFilter = false;

    /**
     * 自动筛选模式的小程序appId或公众号id
     */
    private String appId;
    /**
     * 设为开发模式可以在开发阶段输出请求交互的 xml 与 json 数据,默认false
     */
    private Boolean devMode = false;
    /**
     * #避免本地将线上AccessToken冲掉,上线设置其他值或者改变dev-mode值，该值默认为 fn-weixin-cache#9845s
     */
    private String accessTokenCache="fn-weixin-cache#9845s";
    /**
     * 公众号配置
     */
    private List<ApiConfig> wxConfigs;
    /**
     * 小程序配置
     */
    private List<WxaConfig> wxaConfigs;

    /**
     * 消息解析器
     */
    private WxaMsgParser wxaMsgParser;
    /**
     * 默认解析XML使用jackson
     */
    private String jsonType;

    public FnConfig() {
        this.wxaMsgParser = WxaMsgParser.XML;
        this.jsonType = "jackson";
    }

}
