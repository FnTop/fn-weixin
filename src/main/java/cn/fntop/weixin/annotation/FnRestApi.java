package cn.fntop.weixin.annotation;

import org.springframework.core.annotation.AliasFor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@RequestMapping
@RestController
public @interface FnRestApi {
    /**
     * Alias for {@link RequestMapping#value}.
     *
     * @return {String[]}
     */
    @AliasFor(annotation = RequestMapping.class)
    String[] value() default {};
}
