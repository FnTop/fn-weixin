package cn.fntop.weixin.cache;

import cn.fntop.weixin.config.FnConfig;
import com.jfinal.weixin.sdk.cache.IAccessTokenCache;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

import java.util.Objects;

/**
 * 基于 spring cache 的 weixin token 缓存
 *
 * @author fn
 */
@RequiredArgsConstructor
@Configuration
@Order(1)
public class SpringAccessTokenCache implements IAccessTokenCache {
    private final FnConfig config;
    private final static String ACCESS_TOKEN_PREFIX = "fn:token:";
    private final CacheManager cacheManager;
    private final FnConfig properties;

    private String getKey() {
        StringBuilder buf = new StringBuilder().append(ACCESS_TOKEN_PREFIX);
        if (config.getDevMode()) {
            buf.append("dev");
        }
        return buf.toString();
    }

    @Override
    public String get(String key) {
        return getCache().get(getKey() + key, String.class);
    }

    @Override
    public void set(String key, String jsonValue) {
        getCache().put(getKey() + key, jsonValue);
    }

    @Override
    public void remove(String key) {
        getCache().evict(getKey() + key);
    }

    private Cache getCache() {
        String accessTokenCacheName = properties.getAccessTokenCache();
        Cache cache = cacheManager.getCache(accessTokenCacheName);
        return Objects.requireNonNull(cache, "AccessToken cache is null.");
    }

}
