package cn.fntop.weixin;

import cn.fntop.weixin.utils.WebUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author fn
 * @description
 * @date 2023/3/12 5:24
 */
@SpringBootApplication
public class FnWeixinApp {public static void main(String[] args) {
    SpringApplication.run(WebUtils.class, args);
}
}
